package com.lab.testsystem.services;

import com.lab.testsystem.dao.TestDao;
import lombok.SneakyThrows;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.sql.SQLDataException;
import java.sql.SQLException;
import java.sql.SQLInvalidAuthorizationSpecException;
import java.sql.SQLSyntaxErrorException;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

public class TestServiceImplTest {
    @Mock
    TestDao daoMock;

    @InjectMocks
    TestServiceImpl service;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    @SneakyThrows
    public void testSaveWithValidArgSuccessExpected() {
        when(daoMock.create(new com.lab.testsystem.model.Test())).thenReturn(new com.lab.testsystem.model.Test());
        com.lab.testsystem.model.Test test = new com.lab.testsystem.model.Test();
        assertNotNull(service.save(test));
    }

    @Test(expected = NullPointerException.class)
    @SneakyThrows
    public void testSaveWithNullArgExceptionExpected() {
        service.save(null);
    }

    @Test(expected = IllegalArgumentException.class)
    @SneakyThrows
    public void testSaveWithInvalidArgExceptionExpected() {
        when(daoMock.create(any())).thenThrow(IllegalArgumentException.class);
        service.save(new com.lab.testsystem.model.Test());
    }

    @Test(expected = DatabaseAccessException.class)
    @SneakyThrows
    public void testSaveWithInvalidArgDatabaseAccessExceptionExpected() {
        when(daoMock.create(any())).thenThrow(SQLException.class);
        service.save(new com.lab.testsystem.model.Test());
    }

    @Test(expected = DatabaseAccessException.class)
    @SneakyThrows
    public void testSaveWithInvalidArgDatabaseAccessExceptionExpected2() {
        when(daoMock.create(any())).thenThrow(SQLSyntaxErrorException.class);
        service.save(new com.lab.testsystem.model.Test());
    }

    @Test(expected = DatabaseAccessException.class)
    @SneakyThrows
    public void testSaveWithInvalidArgDatabaseAccessExceptionExpected3() {
        when(daoMock.create(any())).thenThrow(SQLInvalidAuthorizationSpecException.class);
        service.save(new com.lab.testsystem.model.Test());
    }

    @Test(expected = DatabaseAccessException.class)
    @SneakyThrows
    public void testSaveWithInvalidArgDatabaseAccessExceptionExpected4() {
        when(daoMock.create(any())).thenThrow(SQLDataException.class);
        service.save(new com.lab.testsystem.model.Test());
    }

    @Test
    @SneakyThrows
    public void testGetWithValidLongArgSuccessExpected() {
        when(daoMock.get(anyLong())).thenReturn(new com.lab.testsystem.model.Test());
        assertEquals(service.get(anyLong()), new com.lab.testsystem.model.Test());
    }

    @Test(expected = IllegalArgumentException.class)
    @SneakyThrows
    public void testGetWithInValidArgArgExceptionExpected() {
        when(daoMock.get(anyLong())).thenThrow(IllegalArgumentException.class);
        service.get(anyLong());
    }

    @Test(expected = DatabaseAccessException.class)
    @SneakyThrows
    public void testGetWithInValidArgDatabaseAccessExceptionExpected() {
        when(daoMock.get(anyLong())).thenThrow(SQLException.class);
        service.get(anyLong());
    }

    @Test(expected = DatabaseAccessException.class)
    @SneakyThrows
    public void testGetWithInValidArgDatabaseAccessExceptionExpected2() {
        when(daoMock.get(anyLong())).thenThrow(SQLDataException.class);
        service.get(anyLong());
    }

    @Test
    @SneakyThrows
    public void testDeleteWithValidArgSuccessExpected() {
        when(daoMock.delete(any())).thenReturn(new com.lab.testsystem.model.Test());
        assertNotNull(service.delete(new com.lab.testsystem.model.Test()));
    }

    @Test(expected = NullPointerException.class)
    @SneakyThrows
    public void testDeleteWithNullArgSuccessExpected() {
        service.delete(null);
    }

    @Test(expected = DatabaseAccessException.class)
    @SneakyThrows
    public void testDeleteWithInValidArgDatabaseAccessExceptionExpected2() {
        when(daoMock.delete(any())).thenThrow(SQLDataException.class);
        service.delete(new com.lab.testsystem.model.Test());
    }

    @Test
    @SneakyThrows
    public void testGetSuccessExpected() {
        ArrayList<com.lab.testsystem.model.Test> tests = new ArrayList<>();
        tests.add(new com.lab.testsystem.model.Test());
        tests.add(new com.lab.testsystem.model.Test());
        tests.add(new com.lab.testsystem.model.Test());
        when(daoMock.get()).thenReturn(tests);
        assertEquals(service.get().size(), 3);
    }

    @Test(expected = DatabaseAccessException.class)
    @SneakyThrows
    public void testGetDatabaseAccessExceptionExpected() {
        when(daoMock.get()).thenThrow(SQLException.class);
        service.get();
    }

    @Test(expected = DatabaseAccessException.class)
    @SneakyThrows
    public void testGetDatabaseAccessExceptionExpected2() {
        when(daoMock.get()).thenThrow(SQLSyntaxErrorException.class);
        service.get();
    }

    @Test(expected = DatabaseAccessException.class)
    @SneakyThrows
    public void testGetDatabaseAccessExceptionExpected3() {
        when(daoMock.get()).thenThrow(SQLInvalidAuthorizationSpecException.class);
        service.get();
    }


}