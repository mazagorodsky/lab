package com.lab.testsystem.dao;

import com.lab.testsystem.model.Test;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.util.List;

@Component
public class TestDaoImpl implements TestDao {

    @Override
    public Test create(Test t) throws SQLException, IllegalArgumentException {
        return null;
    }

    @Override
    public Test delete(Test t) throws SQLException, IllegalArgumentException {
        return null;
    }

    @Override
    public List<Test> get() throws SQLException {
        return null;
    }

    @Override
    public Test get(long id) throws SQLException, IllegalArgumentException {
        return null;
    }
}
