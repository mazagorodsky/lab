package com.lab.testsystem.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(exclude = {"textAnswer", "isCorrect"})
public class Answer {
    private long id;
    private String textAnswer;
    private boolean isCorrect;
}