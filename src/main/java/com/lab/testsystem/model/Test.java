package com.lab.testsystem.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(exclude = {"title", "questions"})
public class Test {
    private long id;
    private String title;
    private List<Question> questions;
}
