package com.lab.testsystem.services;

import com.lab.testsystem.model.Test;
import com.lab.testsystem.model.TestResult;

import java.util.List;

public interface TestResultService {


    TestResult save(TestResult t);


    TestResult delete(TestResult t);


    List<TestResult> get();


    TestResult get(long id);
}
