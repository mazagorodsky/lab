package com.lab.testsystem.services;

import com.lab.testsystem.model.Test;
import java.util.List;

public interface TestService {

    Test save(Test t);


    Test delete(Test t);


    List<Test> get();


    Test get(long id);

}