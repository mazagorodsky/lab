package com.lab.testsystem.services;

import com.lab.testsystem.dao.TestDao;
import com.lab.testsystem.model.Test;
import lombok.NonNull;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.List;

import static java.util.Objects.isNull;

@Log4j
@Service
public class TestServiceImpl implements TestService {

    @Autowired
    TestDao daoService;

    public Test get(long id) {
        try {
            return daoService.get(id);
        } catch (SQLException e) {
            throw sqlExceptionChecker(e);
        }catch (IllegalArgumentException e) {
            log.error("ID is not valid " + e);
            throw e;
        }
    }

    public Test save(@NonNull Test test) {
        try {
            return daoService.create(test);
        } catch (SQLException e) {
            throw sqlExceptionChecker(e);
        } catch (IllegalArgumentException e) {
            log.error("Test is not valid, test won't be saved " + e.getMessage() + test.toString());
            throw e;
        }
    }


    public Test delete(@NonNull Test test) {
        try {
            return daoService.delete(test);
        } catch (SQLException e) {
            throw sqlExceptionChecker(e);
        } catch (IllegalArgumentException e) {
            log.error("Test is not valid, test won't be deleted " + e.getMessage() + test.toString());
            throw e;
        }
    }

    public List<Test> get() {
        try {
            return daoService.get();
        } catch (SQLException e) {
            throw sqlExceptionChecker(e);
        }
    }

    private DatabaseAccessException sqlExceptionChecker(SQLException exception) {
        if (exception.getClass() == SQLSyntaxErrorException.class) {
            log.error("Query has syntax error(s): " + exception);
        } else if (exception.getClass() == SQLDataException.class) {
            log.error("Data error in db: " + exception);
        } else if (exception.getClass() == SQLInvalidAuthorizationSpecException.class) {
            log.error("Authorization credentials are not valid: " + exception);
        } else if (exception.getClass() == SQLWarning.class) {
            log.warn("Warning: " + exception);
        } else {
            log.error(exception.getClass() + ", error code: " + exception.getErrorCode());
        }
        throw new DatabaseAccessException(exception);
    }
}