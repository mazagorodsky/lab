package com.lab.testsystem.controller;

import com.google.gson.Gson;
import com.lab.testsystem.model.Test;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
public class CreateTestController {
    @RequestMapping(path = "/createTest", method = RequestMethod.GET)
    public ModelAndView createTestDescription() {
        ModelAndView model = new ModelAndView();
        model.setViewName("createTest");
        return model;
    }

    @RequestMapping(path = "/addQuestions", method = RequestMethod.GET)
    public String getMetaData(
        @RequestParam String title, @RequestParam int numberOfQuestions, Model model) {
        model.addAttribute("end", numberOfQuestions);
        return "addQuestions";
    }

    @RequestMapping(path = "/saveTest", method = RequestMethod.POST, consumes = "application/json")
    @ResponseBody
    public String saveTest(@RequestBody String json) {
        Gson gson = new Gson();
        Test test = gson.fromJson(json, Test.class);
        ModelAndView model = new ModelAndView();
        model.setViewName("start-page");
        return "/";
    }

    @RequestMapping(path = "/createTest", method = RequestMethod.POST)
    public String postMetaData(
        HttpServletRequest request, Model model) {
        model.addAttribute("testTitle", request.getParameter("Title"));
        model.addAttribute("questionCount", request.getParameter("numberOfQuestions"));
        model.addAttribute("answerCount", request.getParameter("numberOfAnswers"));
        return "addQuestions";
    }

}