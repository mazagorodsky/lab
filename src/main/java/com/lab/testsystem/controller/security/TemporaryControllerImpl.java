package com.lab.testsystem.controller.security;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Temporary controller for com.lab.testsystem.security demonstraion
 */
@Controller
public class TemporaryControllerImpl {

    // Access only for students
    @RequestMapping(path = "/temp/student", method = RequestMethod.GET)
    @PreAuthorize("hasRole('STUDENT')")
    public ModelAndView studentMethod(Authentication a) {
        ModelAndView model = new ModelAndView();
        model.setViewName("start-page");
        model.addObject("msg", "Student: " + (a == null ? "null" : a.getPrincipal()));
        return model;
    }

    // Access only for tutors
    @RequestMapping(path = "/temp/tutor", method = RequestMethod.GET)
    @PreAuthorize("hasRole('TUTOR')")
    public ModelAndView tutorMethod(Authentication a) {
        ModelAndView model = new ModelAndView();
        model.setViewName("start-page");
        model.addObject("msg", "Tutor: " + (a == null ? "null" : a.getPrincipal()));
        return model;
    }
}
